// component snap grid size - must match $grid-size in _specific.scss
var schematicsGrid = interact.createSnapGrid({
    x: 20,
    y: 20
});

// component drag function
function dragComponent(event) {
    var target = event.target,
        x = (parseFloat(target.getAttribute('data-x')) || 0) + event.dx,
        y = (parseFloat(target.getAttribute('data-y')) || 0) + event.dy;
        r = target.getAttribute('data-r');

    if (r === "true") {
        target.style.webkitTransform = target.style.transform = 'translate(' + x + 'px,' + y + 'px) rotate(90deg)';
    } else {
        target.style.webkitTransform = target.style.transform = 'translate(' + x + 'px, ' + y + 'px) rotate(0deg)';
    }

    target.setAttribute('data-x', x);
    target.setAttribute('data-y', y);
    target.setAttribute('data-r', r);
}

// component resize function
function resizeComponent(event) {
    var target = event.target,
        x = (parseFloat(target.getAttribute('data-x')) || 0),
        y = (parseFloat(target.getAttribute('data-y')) || 0),
        r = target.getAttribute('date-r');

    target.style.width  = event.rect.width + 'px';
    target.style.height = event.rect.height + 'px';

    x += event.deltaRect.left;
    y += event.deltaRect.top;

    if (r === "true") {
        target.style.webkitTransform = target.style.transform = 'translate(' + x + 'px,' + y + 'px) rotate(90deg)';
    } else {
        target.style.webkitTransform = target.style.transform = 'translate(' + x + 'px, ' + y + 'px) rotate(0deg)';
    }

    target.setAttribute('data-x', x);
    target.setAttribute('data-y', y);
    target.setAttribute('data-r', r);
}

// component rotate function
function rotateComponent(event) {
    var target = event.target,
        x = (parseFloat(target.getAttribute('data-x')) || 0),
        y = (parseFloat(target.getAttribute('data-y')) || 0),
        r = target.getAttribute('data-r');

    if (r === "true") {
        target.style.webkitTransform = target.style.transform = 'translate(' + x + 'px,' + y + 'px) rotate(0deg)';
        r = "false";
    } else {
        target.style.webkitTransform = target.style.transform = 'translate(' + x + 'px,' + y + 'px) rotate(90deg)';
        r = "true";
    }

    target.setAttribute('data-r', r);
}

$(function() {

    // components
    interact('.draggable')
        .draggable({
            snap: {
                targets: [schematicsGrid],
                range: Infinity
            },
            restrict: {
                restriction: '.grid',
                elementRect: {
                    left: 0,
                    right: 1,
                    top: 0,
                    bottom: 1
                }
            }
        })
        .resizable({
            snap: {
                targets: [schematicsGrid],
                range: Infinity
            },
            edges: {
                left: true,
                right: true,
                bottom: true,
                top: true
            },
            invert: 'reposition'
        })
        .on('resizemove', resizeComponent)
        .on('dragmove', dragComponent)
        .on('hold', rotateComponent);
});

// angular controllers
function componentCtrl() {
    var vm = this;
    vm.savedData = [];
}

//angular directives
function clickAction() {
    return {
        restrict: 'A',
        scope: {
            event: "@"
        },
        link: function(scope, element, attrs) {
            element.on('click', function() {
                if (element.attr('event') == "remove") {
                    //remove component
                    $(this).parent().remove();
                } else if (element.attr('event') == "add") {
                    //clone component and replace new-component class with draggable
                    var componentType = $(this).children('.new-component').attr('class').split(' ')[0];
                    $(this).children('.new-component').clone(true, true)
                    .attr('class', '' + componentType + ' draggable').appendTo('.grid');
                }
            });
        }
    };
}

function saveSurvey() {
    return {
        restrict: 'A',
        scope: {},
        controller: componentCtrl,
        link: function(scope, element, attrs, componentCtrl) {
            element.on('click', function() {
                //reset array
                componentCtrl.savedData = [];
                //push current components to array
                componentCtrl.savedData.push($('.grid').children('.draggable'));
                console.log(componentCtrl.savedData);
            });
        }
    };
}

// angular bootstrap
angular.module('schematics-survey', [])
    .directive('clickAction', clickAction)
    .directive('saveSurvey', saveSurvey);
