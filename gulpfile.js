/*========================================

	Gulp /w SCSS + PostCSS

	Prerequisites: npm, gulp, postcss

	Setup:
	- Configure the project options
	- Install the node modules with "npm install"
	- Run gulp and browser-sync with "gulp watch"

	Commands:
	gulp watch		Watch for file changes and perform the correct tasks
	gulp styles		Compiles scss and runs postcss processors
	gulp scripts 	Concats and minifys dev js into one file for prod

	Author : Ben Brown

========================================*/

// Browsersync options
var options = {
	'browser-sync': true,
	'domain': 'drag-drop.dev'
};

// Dependencies
var gulp = require('gulp'),
	sass = require('gulp-sass'),
	postcss = require('gulp-postcss'),
	concat = require('gulp-concat'),
	uglify = require('gulp-uglify');

	if (options['browser-sync']) {
		var browserSync = require('browser-sync').create();
	}

// PostCSS Processors
var processors = [
	require('autoprefixer')({ browsers: ['last 2 versions', '> 5%', 'ie 6-8']  }), // Apply vendor prefixes
	require('lost')(), // Lost Grid - https://github.com/corysimmons/lost
	require('rucksack-css'), // Rucksack CSS Extenstions - https://simplaio.github.io/rucksack/
	require('cssnano')({ discardComments: { removeAll: true }}), // Minify CSS
	require('cssnext')() // Usable CSS4 features - http://cssnext.io/
];

// Error handling
function handleError (error) {
	console.log(error.toString());
	this.emit('end');
}

// Watch for style/script changes and sync browser if turned on
gulp.task('watch', function() {
	if (options['browser-sync']) {
		browserSync.init({
			proxy: options.domain
		});
	} else {
		console.log('Not running browser-sync');
	}

	gulp.watch('dev/styles/*.scss', ['styles']);

	if (options['browser-sync']) {
		gulp.watch('dev/scripts/**/*.js', ['scripts']).on('change', browserSync.reload);
		gulp.watch('**/*.php').on('change', browserSync.reload);
	} else {
		gulp.watch('dev/scripts/**/*.js', ['scripts']);
	}
});

// Build styles
gulp.task('styles', function() {
	var build = gulp.src('dev/styles/styles.scss')
		.pipe(sass())
		.pipe(postcss(processors))
		.on('error', handleError)
		.pipe(gulp.dest('prod/styles'));

	if (options['browser-sync']) {
		build = build.pipe(browserSync.stream());
	}

	return build;
});

// Build scripts
gulp.task('scripts', function() {
	return gulp.src('dev/scripts/**/*.js')
		.pipe(concat('js.js'))
		.pipe(uglify())
		.on('error', handleError)
		.pipe(gulp.dest('prod/scripts'));
});
